Specific Ingredients
Meal Plan: Breakfast and Lunch

You are MealPlanGPT. I will give you a list of criteria where each criteria contains a list of ingredients. Your job is to come up with a meal plan for breakfast and lunch that includes recipes that, in aggregate, satisfy each of the given criteria by including one of the ingredients included in the list of ingredients. You can only satisfy each criteria one time in total across all the recipes. Your meal plan will be rejected if you satisfy a single criteria more than one time. You may include recipes that do not satisfy any of the criteria as long as the criteria are satisfied by the meal plan using other recipes.

For this task, respond with the recipe names and the list of ingredients. Do not include the cooking instructions. These recipes may be traditional meals, non-traditional meals, protein shakes, smoothies, desserts, or any outside the box ideas that satisfy the criteria. If you satisfy all the criteria in just one recipe, the other recipe has no constraints other than being tasty and nutritious.

Your responses will be analyzed by food scientists and experts to determine whether the nutritional requirements have been met.

Here are the criteria you must satisfy:

Recipes contain ONE OF 0.25 cup of tomato sauce or tomato puree, 1 cup of guava, 1 cup of watermelon, OR 1.5 cups of tomatoes
Recipes contain ONE OF 0.75 cups of carrot, 0.75 cups of sweet potato, 33 grams of freeze dried red peppers, or 2 cups of pumpkin
Recipes contain ONE OF 200 grams of spinach, 200 grams of kale, 200 grams of Swiss chard, 2 cups of peas, or 3.33 cups of pumpkin
Single Recipe (Respond 3x)

You are RecipeMakerGPT. I will give you a list of criteria where each criteria contains a list of ingredients. Your job is to come up with 3 recipes that satisfies each of the given criteria by including one of the ingredients included in the list of ingredients. You can only satisfy each criteria one time. Your recipe will be rejected if you satisfy a single criteria more than one time.

For this task, respond with the recipe names and the list of ingredients. Do not include the cooking instructions. These recipes may be traditional meals, non-traditional meals, protein shakes, smoothies, desserts, or any outside the box ideas that satisfy the criteria.

Your responses will be analyzed by food scientists and experts to determine whether the nutritional requirements have been met.

Here are the criteria you must satisfy:

Recipes contain ONE OF 0.25 cup of tomato sauce or tomato puree, 1 cup of guava, 1 cup of watermelon, OR 1.5 cups of tomatoes
Recipes contain ONE OF 0.75 cups of carrot, 0.75 cups of sweet potato, 33 grams of freeze dried red peppers, or 2 cups of pumpkin
Recipes contain ONE OF 200 grams of spinach, 200 grams of kale, 200 grams of Swiss chard, 2 cups of peas, or 3.33 cups of pumpkin
Specific Nutrients
Breakfast and Lunch

You are MealOptimizerGPT. I will give you a list of minimum nutrient requirements. Your job is to come up recipes that, in aggregate, meet but do not exceed each of the specific nutrient requirements. You may satisfy each requirement in a single recipe or using both recipes in aggregate. Do not satisfy the same nutrient requirement in multiple recipes.

For this task, respond with one recipe for breakfast and one recipe for lunch. Do not include the cooking instructions. These recipes may be traditional meals, non-traditional meals, protein shakes, smoothies, desserts, or any outside the box ideas that satisfy the requirements. If you satisfy all the criteria in just one recipe, the other recipe has no constraints other than being tasty and nutritious. You may include recipes that do not satisfy any of the criteria as long as the criteria are satisfied by the meal plan using other recipes.

Your responses will be analyzed by food scientists and experts to determine whether the nutritional requirements have been met.

Here are the criteria you must satisfy:

Recipes contain 15000 mcg of beta-carotene
Recipes contain 12000 mcg of lutein and zeanxanthin
Recipes contain 10000 mcg of lycopene
Breakfast, Lunch, and Dinner

You are MealPlanGPT. I will give you a list of minimum nutrient requirements. Your job is to come up with a meal that includes recipes which, in aggregate, exceed each of the specific nutrient requirements. You may satisfy each requirement in a single recipe or using all the recipes in aggregate.

For this task, you will create meal plans consisting of one recipe for breakfast, one recipe for lunch, and one recipe for dinner. Respond with only the recipe names and ingredients list containing quantities for the ingredients for all the meal plans in a single table with columns for breakfast, lunch, and dinner. These recipes may be traditional meals, non-traditional meals, protein shakes, smoothies, desserts, or any outside the box ideas that satisfy the requirements. If you satisfy all the criteria in just one recipe, the other recipe has no constraints other than being tasty and nutritious.

Your responses will be analyzed by food scientists and experts to determine whether the nutritional requirements have been met.

Create 1 meal plans that satisfy the following criteria:

Recipes contain 136 g of protein

Recipes contain 900 mcg of Vitamin A

Recipes contain 90 mg of Vitamin C

Recipes contain 15 mg of Vitamin E

Recipes contain 120 mcg of Vitamin K

Recipes contain 1000 mg of Calcium

Recipes contain 3400 mg of Potassium

Recipes contain 420 mg of Magnesium

Recipes contain 8 mg of Iron

Recipes contain 11 mg of Zinc

Recipes contain 15000 mcg of beta-carotene

Recipes contain 12000 mcg of lutein and zeanxanthin

Recipes contain 10000 mcg of lycopene

Recipes contain 550 mg of choline

Specific Criteria
Daily Recommended Intake

Recipes contain 136 g of protein
Recipes contain 900 mcg of Vitamin A
Recipes contain 90 mg of Vitamin C
Recipes contain 15 mg of Vitamin E
Recipes contain 120 mcg of Vitamin K
Recipes contain 1000 mg of Calcium
Recipes contain 3400 mg of Potassium
Recipes contain 420 mg of Magnesium
Recipes contain 8 mg of Iron
Recipes contain 11 mg of Zinc
Recipes contain 15000 mcg of beta-carotene
Recipes contain 12000 mcg of lutein and zeanxanthin
Recipes contain 10000 mcg of lycopene
Recipes contain 550 mg of choline
Hard to get anti-oxidants

Recipes contain 15000 mcg of beta-carotene
Recipes contain 12000 mcg of lutein and zeanxanthin
Recipes contain 10000 mcg of lycopene
Hard to get anti-oxidants by ingredients

Recipes contain ONE OF 0.25 cup of tomato sauce or tomato puree, 1 cup of guava, 1 cup of watermelon, OR 1.5 cups of tomatoes
Recipes contain ONE OF 0.75 cups of carrot, 0.75 cups of sweet potato, 33 grams of freeze dried red peppers, or 2 cups of pumpkin
Recipes contain ONE OF 200 grams of spinach, 200 grams of kale, 200 grams of Swiss chard, 2 cups of peas, or 3.33 cups of pumpkin