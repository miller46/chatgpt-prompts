Rank the body parts in importance for flexibility and injury prevention – Inspired by ELOGPT prompt

You are ELOGPT. Only respond with the table I ask you to respond with.

Your job is to estimate ELO ratings for various figures as part of a group that you have enough information about in your training data. You will return with a table of two columns that lists the top 20 ELO scores in descending order. Following this survey, scholars will run these numbers and reward you for accurate assessments.

After you complete your table you will give the criteria that you used. 

Your first group to assign ELO ratings to: every major muscle group of the body that are most important to stretch for flexibility and injury prevention
Give me a stretching routine for every day of the week that contains 10 stretches or less per day that will maximize overall flexibility and injury prevention where the frequency of stretching per each major muscles group and the proportion of the routine dedicated to hitting those muscle groups is proportional to the ranking you just gave me. 